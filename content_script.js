function skipYouku() {  // 跳过优酷视频广告
    window.onload = ()=>{
        var adLayer = document.querySelector('.h5-ext-layer') || document.querySelector('.advertise-layer');
        var config = {attributes: true, childList: true, subtree: true};
        var callback = ()=>{
            if (adLayer.children.length > 1) {
                let videos = adLayer.querySelectorAll('video');  // 获取adPlayer中的广告视频标签
                if (videos.length > 0) {
                    videos.forEach(v=>{
                        if (!isNaN(v.duration)) {
                            v.currentTime=v.duration;
                        }
                    });
                }
            }
        };
        var observer = new MutationObserver(callback);
        observer.observe(adLayer, config);
    };
}

function skipIqiyi() {  // 跳过爱奇艺视频广告
    function skip() {
        var adPlayer = document.querySelector('.cupid-public-time');
        if (adPlayer) {
            if (adPlayer.style.display != 'none') {
                document.querySelector('.skippable-after').click();
            }
        }
    }
    skip();
    setInterval(skip, 1000);
}

function accelerateQQ() {  // 能将广告时间减少86%以上
    var rate = 4;  // 加速不能超过4倍速
    function skip() {
        var adPlayers = document.querySelectorAll('.txp_ad');
        adPlayers.forEach((adPlayer)=>{
            if (adPlayer.classList.contains('txp_none')) {  // 广告播放完毕
                return;
            }
            var videos = adPlayer.querySelectorAll('video');
            videos.forEach(v=>{
                if (v.playbackRate < rate) v.playbackRate = rate;
                if (v.paused) v.play();
            });
        });
    }
    skip();
    setInterval(skip, 1000);
}

function skipSohu() {
    var id;
    function skip() {
        var adv = document.querySelector('.x-video-adv');
        if (adv) {
            var video = adv.querySelector('video');
            if (video && !isNaN(video.duration)) {
                video.currentTime = video.duration;
                return;
            }
        }
        clearInterval(id);
    }
    skip();
    setInterval(skip, 1000);
}

function skipLeshi() {
    function skip() {
        var video = document.querySelector('video');
        if (document.querySelectorAll('.vdo_post_rlt').length) {
            video.playbackRate = 7;
        } else {
            video.playbackRate = 1;
        }
    }
    skip();
    setInterval(skip, 1000);
}

function skipMgtv() {
    function skip() {
        var video = document.querySelector('video');
        if (document.querySelectorAll('.as_video-modal_group.as_video-modal_group--top.as_video-modal_row.as_clear').length && !isNaN(video.duration)) {
            video.currentTime = video.duration;
        }
    }
    skip();
    setInterval(skip, 1000);
}

function skipPPTV() {
    function skip() {
        var ad = document.querySelector('.w-video-vastad');
        if (ad) {
            ad.querySelectorAll('video').forEach(e=>{
                if (!isNaN(e.duration)) {
                    e.currentTime=e.duration;
                }
            });
        }
    }
    skip();
    setInterval(skip, 1000);
}

function skipMigu() {
    function skip() {
        let adver_box = document.querySelector('#adver-box');
        if (adver_box) {
            adver_box.querySelectorAll('video').forEach(e=>{
                if (!isNaN(e.duration)) {
                    e.currentTime = e.duration;
                }
                if (e.paused) {
                    e.play()
                }
            });
        }
    }
    skip();
    setInterval(skip, 1000);
}

function main() {
    
        let domain = window.location.href.match(/(?<=https*:\/\/)\w+?.\w+?.\w+?\/./);
        if (domain) {
            switch (domain.toString()) {
                case 'v.youku.com/v':
                    skipYouku();
                    break;
                case 'www.iqiyi.com/v':
                    skipIqiyi();
                    break;
                case 'v.qq.com/x':
                case 'v.qq.com/l':
                    accelerateQQ();
                    break;
                case 'tv.sohu.com/v':
                    skipSohu();
                    break;
                case 'www.le.com/p':
                    skipLeshi();
                    break;
                case 'www.mgtv.com/b':
                    skipMgtv();
                    break;
                case 'v.pptv.com/s':
                    skipPPTV();
                    break;
                case "www.miguvideo.com/p":
                    skipMigu();
                    break;
                default: console.log('did not match');
            }
        }
    
}

main();
